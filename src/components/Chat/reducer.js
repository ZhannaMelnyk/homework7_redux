import {
  SUCCESSFUL_FETCH_MESSAGES,
  CREATE_MESSAGE,
  DELETE_MESSAGE,
  LIKE_MESSAGE,
  SHOW_EDIT_MODAL,
  HIDE_EDIT_MODAL,
  SAVE_EDITED_MESSAGE
} from './actionTypes'

const initialState = {
  currentUser: {
    userId: '9e243930-83c9-11e9-8e0c-8f1a686f4ce4',
    user: "Ruth",
    avatar: "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA",
  },
  messages: [],
  editedMessage: {
    id: '',
    text: ''
  },
  isEditModalShown: false
}

export const chatReducer = (state = initialState, action) => {
  switch (action.type) {
    case SUCCESSFUL_FETCH_MESSAGES:
      return { ...state, messages: action.messages };

    case CREATE_MESSAGE: {
      const newMessage = Object.assign({}, action.message, state.currentUser)
      const updatedMessages = state.messages.concat(newMessage);

      return { ...state, messages: updatedMessages };
    }

    case DELETE_MESSAGE:
      return { ...state, messages: state.messages.filter(message => message.id !== action.messageId) }

    case LIKE_MESSAGE:
      return {
        ...state, messages: state.messages.map(message => {
          return message.id === action.messageId
            ? { ...message, isLiked: !message.isLiked }
            : message
        })
      }

    case SHOW_EDIT_MODAL: {
      const editedMessage = {
        id: action.messageId,
        text: action.messageText
      }

      return { ...state, editedMessage: editedMessage, isEditModalShown: true };
    }

    case HIDE_EDIT_MODAL: {
      return { ...state, editedMessageId: '', isEditModalShown: false };
    }

    case SAVE_EDITED_MESSAGE: {
      return {
        ...state, messages: state.messages.map(message => {
          return message.id === action.messageId
            ? {
              ...message,
              text: action.messageText,
              editedAt: new Date().toISOString()
            }
            : message
        })
      }
    }

    default:
      return state;
  }
}