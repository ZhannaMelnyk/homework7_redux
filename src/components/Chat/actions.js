import {
  SUCCESSFUL_FETCH_MESSAGES,
  CREATE_MESSAGE,
  DELETE_MESSAGE,
  LIKE_MESSAGE,
  SHOW_EDIT_MODAL,
  HIDE_EDIT_MODAL,
  SAVE_EDITED_MESSAGE
} from './actionTypes';

export const successfulFetchMessagesAction = messages => {
  return {
    type: SUCCESSFUL_FETCH_MESSAGES,
    messages
  }
};

export const createMessageAction = newMessage => {
  return {
    type: CREATE_MESSAGE,
    message: newMessage
  }
};

export const deleteMessageAction = messageId => {
  return {
    type: DELETE_MESSAGE,
    messageId
  }
}

export const likeMessageAction = messageId => {
  return {
    type: LIKE_MESSAGE,
    messageId
  }
}

export const showEditModalAction = (messageId, messageText) => {
  return {
    type: SHOW_EDIT_MODAL,
    messageId,
    messageText
  }
}

export const hideEditModalAction = () => {
  return {
    type: HIDE_EDIT_MODAL
  }
}

export const saveEditedMessageAction = (messageId, messageText) => {
  return {
    type: SAVE_EDITED_MESSAGE,
    messageId,
    messageText
  }
}