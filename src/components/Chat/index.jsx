import React from 'react';
import PageHeader from '../PageHeader';
import ChatHeader from '../ChatHeader';
import MessageList from '../MessageList';
import MessageInput from '../MessageInput';
import EditModal from '../EditModal';
import Loader from '../Loader';
import { getData } from '../../helpers/dataHelper';
import compare from '../../helpers/sortHelper';
import getFormatedTime from '../../helpers/formatDateHelper';
import createMessageId from '../../helpers/idHelper';
import { ARROW_UP } from '../../constants';
import { connect } from 'react-redux';
import {
  successfulFetchMessagesAction,
  createMessageAction,
  deleteMessageAction,
  likeMessageAction,
  showEditModalAction,
  hideEditModalAction,
  saveEditedMessageAction
} from './actions';

import './index.css';

class Chat extends React.Component {
  componentDidMount() {
    this.checkData();
  }

  checkData() {
    getData()
      .then(response => this.props.successFetchMessages(response.sort(compare)))
  }

  getUniqueUsersAmount() {
    let uniqueUsers = [];

    this.props.messages.map(message => {
      return uniqueUsers.find(value => value === message.userId)
        ? null
        : uniqueUsers.push(message.userId);
    })

    return uniqueUsers.length;
  }

  getLastMessageTime() {
    const lastMessageIndex = this.props.messages.length - 1;
    const lastMessage = this.props.messages[lastMessageIndex];

    return getFormatedTime(lastMessage.createdAt)
  }

  createMessage = messageText => {
    const newMessage = {
      id: createMessageId(),
      text: messageText,
      editedAt: '',
      createdAt: new Date().toISOString()
    }

    this.props.createMessage(newMessage);
  }

  deleteMessage = id => {
    this.props.deleteMessage(id);
  }

  likeMessage = id => {
    this.props.likeMessage(id);
  }

  editMessage = (id, text) => {
    this.props.showEditModal(id, text);
  }

  hideEditModal = () => {
    this.props.hideEditModal();
  }

  saveEditedMessage = (id, text) => {
    this.props.saveEditedMessage(id, text);
    this.hideEditModal();
  }

  editLastMessageFromCurrentUser = (event) => {
    if (event.code === ARROW_UP && !this.props.isEditModalShown) {
      document.removeEventListener('keydown', this.editLastMessageFromCurrentUser)

      const { id, text } = this.props.messages.reverse()
        .find(message => message.userId === this.props.currentUser.userId);
      this.props.messages.reverse();
      this.editMessage(id, text)
    }
  }

  render() {
    document.addEventListener('keydown', this.editLastMessageFromCurrentUser)
    return (
      <>
        {
          this.props.messages.length > 0
            ? <>
              <PageHeader
                userName={this.props.currentUser.user}
                avatar={this.props.currentUser.avatar} />
              <div className='chat-container'>
                <ChatHeader
                  usersAmount={this.getUniqueUsersAmount()}
                  messageAmount={this.props.messages.length}
                  lastMessageTime={this.getLastMessageTime()} />
                <MessageList
                  messageList={this.props.messages}
                  currentUser={this.props.currentUser}
                  deleteMessage={this.deleteMessage}
                  editMessage={this.editMessage}
                  likeMessage={this.likeMessage} />
                <MessageInput createMessage={this.createMessage} />
              </div>
              <footer className='footer'>
                2020 by Zhanna Melnyk
              </footer >
              {
                this.props.isEditModalShown
                && <EditModal
                  messageId={this.props.editedMessage.id}
                  previousMessageText={this.props.editedMessage.text}
                  hideEditModal={this.hideEditModal}
                  saveEditedMessage={this.saveEditedMessage} />
              }
            </>
            : <Loader />
        }
      </>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    currentUser: state.chatReducer.currentUser,
    messages: state.chatReducer.messages,
    isEditModalShown: state.chatReducer.isEditModalShown,
    editedMessage: state.chatReducer.editedMessage
  }
}

const mapDispatchToProps = dispatch => {
  return {
    successFetchMessages: (messages) => dispatch(successfulFetchMessagesAction(messages)),
    createMessage: (newMessage) => dispatch(createMessageAction(newMessage)),
    deleteMessage: (messageId) => dispatch(deleteMessageAction(messageId)),
    likeMessage: (messageId) => dispatch(likeMessageAction(messageId)),
    showEditModal: (messageId, messageText) => dispatch(showEditModalAction(messageId, messageText)),
    hideEditModal: () => dispatch(hideEditModalAction()),
    saveEditedMessage: (messageId, messageText) => dispatch(saveEditedMessageAction(messageId, messageText))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);