import React from 'react';
import PropTypes from 'prop-types';
import isMessageValid from '../../helpers/messageValidateHelper';

import './index.css';

class EditModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = { messageText: this.props.previousMessageText };
  }

  handleChange = (event) => {
    this.setState({ messageText: event.target.value });
  }

  render() {
    return <div className='modal__layer'>
      <div className='modal__container'>
        <textarea
          className='input modal__message-input'
          placeholder='Enter message'
          value={this.state.messageText}
          onChange={this.handleChange} />

        <button
          className='btn modal__btn modal__cancel-btn'
          onClick={() => this.props.hideEditModal()}>Cancel</button>
          
        <button className='btn modal__btn modal__save-btn'
          onClick={() => {
            if (isMessageValid(this.state.messageText)) {
              this.props.previousMessageText === this.state.messageText
              ? this.props.hideEditModal()
              : this.props.saveEditedMessage(this.props.messageId, this.state.messageText)
            }
          }}>Save</button>
      </div>
    </div>
  }
}

EditModal.propTypes = {
  messageId: PropTypes.string,
  previousMessageText: PropTypes.string,
  hideEditModal: PropTypes.func
}

export default EditModal;
