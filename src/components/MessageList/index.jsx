import React from 'react';
import PropTypes from 'prop-types';
import Message from '../Message';
import setScrollToDown from '../../helpers/scrollHelper';
import Separator from '../Separator';

import './index.css';

class MessageList extends React.Component {
  render() {
    setScrollToDown();

    let lastMessageDate = '';
    return <div className='message-list'>
      {
        this.props.messageList.map(message => {
          const date = new Date(message.createdAt).toDateString();
          const components = [];

          if (date !== lastMessageDate) {
            components.push(
              <Separator date={date} />
            )
          }
          
          lastMessageDate = date;
          components.push(
            <Message
              key={message.id}
              message={message}
              currentUser={this.props.currentUser}
              deleteMessage={this.props.deleteMessage}
              editMessage={this.props.editMessage}
              likeMessage={this.props.likeMessage} />
          )
          return components
        })
      }
    </div>
  }
}

MessageList.propTypes = {
  messageList: PropTypes.arrayOf(PropTypes.object),
  currentUser: PropTypes.objectOf(PropTypes.any),
  deleteMessage: PropTypes.func,
  editMessage: PropTypes.func,
  likeMessage: PropTypes.func
}

export default MessageList;