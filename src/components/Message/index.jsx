import React from 'react';
import PropTypes from 'prop-types';
import getFormatedTime from '../../helpers/formatDateHelper'

import edit from '../../assets/edit-solid.svg';
import trash from '../../assets/trash-alt-solid.svg';
import like from '../../assets/heart-regular.svg';
import likeSolid from '../../assets/heart-solid.svg';

import './index.css';

class Message extends React.Component {
  render() {
    const { id, text, avatar, userId, createdAt, editedAt, isLiked } = this.props.message;
    return this.props.currentUser.userId === userId
      ? (
        <div className='message message-right'>
          <div className='message__content'>
            <p className='message__content-text'>{text}</p>
            <span className='message__content-date'>
              {getFormatedTime(createdAt)} {editedAt ? `(edited: ${getFormatedTime(editedAt)})` : null}
            </span>
            <div className='message__content-btns'>
              <img src={edit} onClick={() => this.props.editMessage(id, text)} alt='edit' />
              <img src={trash} onClick={() => this.props.deleteMessage(id)} alt='trash' />
            </div>
          </div>
        </div>
      )
      : (
        <div className='message message-left' >
          <img src={avatar} className='message__avatar' alt='avatar' />
          <div className='message__content'>
            <p className='message__content-text'>{text}</p>
            <span className='message__content-date'>
              {getFormatedTime(createdAt)} {editedAt ? `(edited: ${getFormatedTime(editedAt)})` : null}
            </span>
            <div className='message__content-btns'>
              {
                isLiked
                  ? <img src={likeSolid} onClick={() => this.props.likeMessage(id)} alt='like' />
                  : <img src={like} onClick={() => this.props.likeMessage(id)} alt='like' />
              }
            </div>
          </div>
        </div >
      )
  }
}

Message.propTypes = {
  message: PropTypes.objectOf(PropTypes.any),
  currentUser: PropTypes.objectOf(PropTypes.any),
  deleteMessage: PropTypes.func,
  editMessage: PropTypes.func,
  likeMessage: PropTypes.func
}

export default Message;