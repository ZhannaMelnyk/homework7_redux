export default function compare(a, b) {
  const bandA = a.createdAt;
  const bandB = b.createdAt;

  let comparison = 0;
  if (bandA > bandB) {
    comparison = 1;
  } else if (bandA < bandB) {
    comparison = -1;
  }
  return comparison;
}