export const getData = async () => {
  const data = await fetch('https://edikdolynskyi.github.io/react_sources/messages.json');
  const result = await data.json();
  return result;
};
