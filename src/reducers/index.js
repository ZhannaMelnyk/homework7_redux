import { combineReducers } from 'redux';
import { chatReducer } from '../components/Chat/reducer';

const rootReducer = combineReducers({
  chatReducer
});

export default rootReducer;